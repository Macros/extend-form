;(function ( $, window, document, undefined ) {
    // Create the defaults once
    var pluginName = 'extendForm',
        defaults = {
            prefixName: '',
            suffixName: '',
            validateOnSubmit: false,
            validateOnKeyUp: false,
            rules: [],
            method: 'POST',
            dataType: 'json',
            url: null,
            shortNames: false,
            onError: function(input, rule) {
                input.element.addClass('error');
            },
            onValid: function(input) {
                input.element.removeClass('error');
            },
            onSuccess: function(input) {
            },
            onBeforeSend: null
        },
        defaultRule = {
            skipNull: true,
            validateOnBlur: true,
            message: ''
        };

    // The actual plugin constructor
    function Plugin(element, options) {
        this.inputMapName = [];
        this.inputMap = [];
        this.element = element;

        this.options = $.extend({}, defaults, options) ;
        
        this._defaults = defaults;
        this._name = pluginName;
        
        this.init();
    }

    Plugin.prototype.initOldValues = function(data) {
        if (data === undefined)
            data = this.getData(true);
        this.oldValues = data;
    }

    Plugin.prototype.getRule = function(rule, options) {
        return $.extend({'rule': rule}, defaultRule, options);
    }

    Plugin.prototype.isChanged = function(data) {
        for (let i = 0; i < this.inputMapName.length; i++) {
            if (this.oldValues[this.inputMapName[i]] != data[this.inputMapName[i]])
                return true;
        }

        return false;
    }

    Plugin.prototype.addMap = function(item, rule, options) {
        let index = this.inputMapName.indexOf(item), _rule = null;
        var name = this.options.prefixName + item + this.options.suffixName;

        if (index === -1) {
            let element = $(this.element).find('[name="' + name + '"]');
            if (element.length) {
                index = this.inputMapName.length;
                this.inputMapName.push(item);
                this.inputMap.push({
                    'name': name,
                    'element': element,
                    'valid': null,
                    'item': item,
                    'rules': [],
                    'blur': []
                })
            } else {
                console.log('Warning: DOM element with name "' + name + '" not found!');
                return false;
            }
        }
        _rule = this.getRule(rule, options);
        this.inputMap[index].rules.push(_rule);

        if (_rule.validateOnBlur)
            this.inputMap[index].blur.push(_rule);

    }

    Plugin.prototype.getInput = function(name) {
        let index = this.inputMapName.indexOf(name);
        return index > -1 ? this.inputMap[index] : null;
    }

    Plugin.prototype.getData = function(short) {
        var data = {}, name;

        for (let i = 0; i < this.inputMap.length; i++) {
            name = short ? this.inputMapName[i] : this.inputMap[i].name;
            data[name] = this.inputMap[i].element.val();
        }

        return data;
    }

    Plugin.prototype.initMap = function() {
        let rules = this.options.rules;

        for (let i = 0; i < rules.length; i++) {
            if (Array.isArray(rules[i][0])) {
                for (let j = 0; j < rules[i][0].length; j++) {
                    this.addMap(rules[i][0][j], rules[i][1], rules[i][2]);
                }
            } else {
                this.addMap(rules[i][0], rules[i][1], rules[i][2]);
            }
        }
    }
    
    Plugin.prototype.validateInput = function(input, rules, data) {
        if (data === null)
            data = this.getData(true);
        var result = validateValue(input.element.val(), rules, data);
        input.valid = result === true;

        if (result === true) {
            return this.options.onValid(input.element);
        }

        return this.options.onError(input.element, result);
    }

    /**
     * 
     * @param {string} value 
     * @param {array} rules 
     * @returns true|rule
     */
    function validateValue(value, rules, data) {
        for (let i = 0; i < rules.length; i++) {
            if (!validate(value, rules[i], data))
                return rules[i];
        }

        return true;
    }

    function validate(value, rule, data) {
        if (rule.rule == 'required') {
            return !!value.length;
        }

        if (rule.skipNull && (value === null || !value.length))
            return true;

        if (rule.rule == 'pattern') {
            return rule.regexp.test(value);
        }

        if (rule.rule == 'string') {
            if (rule.min !== undefined && value.length < rule.min)
                return false;
            if (rule.max !== undefined && value.length > rule.max)
                return false;
            return true;
        }

        if (rule.rule == 'number') {
            if (!isNaN(value)) {
                if (rule.min !== undefined && value < rule.min)
                    return false;
                if (rule.max !== undefined && value > rule.max)
                    return false;
                return true;
            }

            return false;
        }

        if (rule.rule == 'in') {
            return rule.array.indexOf(value) > -1;
        }

        if (rule.rule == 'equals') {
            return value == data[rule.field];
        }

        if (rule.rule == 'ajax') {
            var result = null;
            $.ajax({
                'type': rule.method || 'GET',
                'dataType': rule.dataType || 'json',
                'url': rule.url,
                'async': false,
                'data': rule.data || {},
                'timeout': rule.timeout || 1000,
                'success': function (response) {
                    result = rule.success(response);
                },
                'error': function(xmlhttprequest, textstatus, message) {
                    if (textstatus === "timeout")
                        result = rule.onTimeout || true;
                }
            });

            return !!result;
        }

        if (rule.rule == 'callback') {
            return rule.callback(value, rules, data);
        }

        return true;
    }

    Plugin.prototype.initEvents = function() {
        for (let i = 0; i < this.inputMap.length; i++) {
            if (this.options.validateOnKeyUp) {
                this.inputMap[i].element.keyup(this.validateInput.bind(this, this.inputMap[i], this.inputMap[i].blur, null));
            }
            if (this.inputMap[i].blur.length) {
                this.inputMap[i].element.blur(this.validateInput.bind(this, this.inputMap[i], this.inputMap[i].blur, null));
            }
        }
    }

    Plugin.prototype.isValid = function() {
        for (let i = 0; i < this.inputMap.length; i++) {
            if (this.inputMap[i].valid === false)
                return false;
        }

        return true;
    }

    Plugin.prototype.validateForm = function(data) {
        var result, data = this.getData(true);
        
        for (let i = 0; i < this.inputMap.length; i++) {
            if (this.options.validateOnSubmit || (this.inputMap[i].valid !== true)) {
                this.validateInput(this.inputMap[i], this.inputMap[i].rules, data);
            }
        }
    }

    Plugin.prototype.init = function() {
        this.initMap();
        this.initEvents();
        if (this.options.onBeforeSend !== null) {
            this.options.onBeforeSend.bind(this);
        }

        $(this.element).submit(function(e) {
            this.validateForm();

            if (!this.isValid()) {
                e.preventDefault();
                return false;
            }

            if (this.options.ajax) {
                e.preventDefault();
                var data = this.getData(this.options.shortNames);

                if (this.options.onBeforeSend === null || this.options.onBeforeSend(data)) {
                    $.ajax({
                        'type': this.options.method || $(this.element).attr('method') || 'GET',
                        'dataType': this.options.dataType ||'json',
                        'url': this.options.url || $(this.element).attr('action'),
                        'async': false,
                        'data': data,
                        'success': this.options.onSuccess.bind(this, data)
                    });
                }
            }
        }.bind(this));
    };

    // A really lightweight plugin wrapper around the constructor, 
    // preventing against multiple instantiations
    $.fn[pluginName] = function(options) {
        if (typeof arguments[0] === 'string') {
            var returnVal, methodName = arguments[0];
            var args = Array.prototype.slice.call(arguments, 1);
            this.each(function() {
                if ($.data(this, 'plugin_' + pluginName) && typeof $.data(this, 'plugin_' + pluginName)[methodName] === 'function') {
                    returnVal = $.data(this, 'plugin_' + pluginName)[methodName](args);
                } else {
                    throw new Error('Method ' +  methodName + ' does not exist on jQuery.' + pluginName);
                }
            });

            if (returnVal !== undefined){
                return returnVal;
            } else {
                return this;
            }
        } else {
            return this.each(function () {
                if (!$.data(this, 'plugin_' + pluginName)) {
                    $.data(this, 'plugin_' + pluginName,
                    new Plugin(this, options));
                }
            });
        }
    }
})(jQuery, window, document);